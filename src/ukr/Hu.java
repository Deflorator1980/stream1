package ukr;

import java.util.*;

/**
 * Created by a on 19.04.16.
 */
public class Hu {
    public static void main(String[] args) {


        List<Person> persons =
                Arrays.asList(
                        new Person("Andrew", 20),
                        new Person("Igor", 23),
                        new Person("Ira", 23),
                        new Person("Vitia", 12));
                persons.parallelStream()
                        .reduce(0,
                                (sum, p) -> {
                                    System.out.format("accumulator: sum=%s; person=%s [%s]\n",
                                            sum, p, Thread.currentThread().getName());
                                    return sum += p.age;
                                },
                                (sum1, sum2) -> {
                                    System.out.format("combiner: sum1=%s; sum2=%s [%s]\n",
                                            sum1, sum2, Thread.currentThread().getName());
                                    return sum1 + sum2;
                                });
//        Arrays.asList("a1", "a2", "b1", "c2", "c1")
//                .parallelStream()
//                .filter(s -> {
//                    System.out.format("filter: %s [%s]\n",
//                            s, Thread.currentThread().getName());
//                    return true;
//                })
//                .map(s -> {
//                    System.out.format("map: %s [%s]\n",
//                            s, Thread.currentThread().getName());
//                    return s.toUpperCase();
//                })
//                .sorted((s1, s2) -> {
//                    System.out.format("sort: %s <> %s [%s]\n",
//                            s1, s2, Thread.currentThread().getName());
//                    return s1.compareTo(s2);
//                })
//                .forEach(s -> System.out.format("forEach: %s [%s]\n",
//                        s, Thread.currentThread().getName()));
////        List<ukr.Person> persons =
//                Arrays.asList(
//                        new ukr.Person("Andrew", 20),
//                        new ukr.Person("Igor", 23),
//                        new ukr.Person("Ira", 23),
//                        new ukr.Person("Vitia", 12));
//
//        ForkJoinPool fjp = ForkJoinPool.commonPool();
//        System.out.println(fjp.getParallelism());
//----------------
//        Integer ageSum = persons
//                .stream()
//                .reduce(0, (sum, p) -> sum += p.age, (sum1, sum2) -> sum1 + sum2);
//        System.out.println(ageSum);
//        Integer ageSum = persons
//                .stream()
//                .reduce(0,
//                        (sum, p) -> {
//                            System.out.format("accumulator: sum=%s; person=%s\n", sum, p);
//                            return sum += p.age;
//                        },
//                        (sum1, sum2) -> {
//                            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
//                            return sum1 + sum2;
//                        });
//        Integer ageSum = persons
//                .parallelStream()
//                .reduce(0,
//                        (sum, p) -> {
//                            System.out.format("accumulator: sum=%s; person=%s\n", sum, p);
//                            return sum += p.age;
//                        },
//                        (sum1, sum2) -> {
//                            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
//                            return sum1 + sum2;
//                        });

//accumulator: sum=0; person=Andrew
//accumulator: sum=20; person=Igor
//accumulator: sum=43; person=Ira
//accumulator: sum=66; person=Vitia

//        ukr.Person result = persons
//                .stream()
//                .reduce(new ukr.Person("", 0), (p1, p2) -> {
//                    p1.age += p2.age;
//                    p1.name += p2.name;
//                    return p1;
//                });
//        System.out.format("name=%s; age=%s", result.name, result.age);

//        persons
//                .stream()
//                .reduce((p1, p2) -> p1.age > p2.age ? p1 : p2)
//                .ifPresent(System.out::println);
//------
//        ukr.Outer outer = new ukr.Outer();
//        ukr.Nested nested = new ukr.Nested();
//        ukr.Inner inner = new ukr.Inner();
//        inner.setFoo("hui");
//        outer.setNested(nested);
//        nested.setInner(inner);
//
////        if (outer != null && outer.nested != null && outer.nested.inner != null) {
////            System.out.println(outer.nested.inner.foo);
////        } else System.out.println("ni huya");
//
////        Optional.of(new ukr.Outer())
//        Optional.of(outer)
//                .flatMap(o -> Optional.ofNullable(o.nested))
//                .flatMap(n -> Optional.ofNullable(n.inner))
//                .flatMap(i -> Optional.ofNullable(i.foo))
//                .ifPresent(System.out::println);

//        IntStream.range(1, 4)
//                .mapToObj(i -> new ukr.Foo("ukr.Foo" + i))
//                .peek(f -> IntStream.range(1, 4)
//                        .mapToObj(i -> new ukr.Bar("ukr.Bar" + i + " <- " + f.name))
//                        .forEach(f.bars::add))
//                .flatMap(f -> f.bars.stream())
//                .forEach(b -> System.out.println(b.name));


//        List<ukr.Foo> foos = new ArrayList<>();
//
//        IntStream
//                .range(1, 4)
//                .forEach(i -> foos.add(new ukr.Foo("ukr.Foo" + i)));
//
//        foos.forEach(f ->
//                IntStream
//                        .range(1, 4)
//                        .forEach(i -> f.bars.add(new ukr.Bar("ukr.Bar"+i+" <- "+f.name)))
//        );
//
//        foos.stream()
//                .flatMap(f -> f.bars.stream())
//                .forEach(b -> System.out.println(b.name));
//

//
//        Collector<ukr.Person, StringJoiner, String> pnc =
//            Collector.of(
//                    () -> new StringJoiner(" | "),
//                    (j, p) -> j.add(p.name.toUpperCase()),
//                    (j1, j2) -> j1.merge(j2),
//                    StringJoiner::toString
//            );
//
//        String names = persons
//                .stream()
//                .collect(pnc);
//        System.out.println(names);

//        String phrase = persons
//                .stream()
//                .filter(p -> p.age >= 18)
//                .map(p -> p.name)
//                .collect(Collectors.joining(" и ", "В Германии ", " совершеннолетние."));
//        System.out.println(phrase);
//        System.out.println(persons);

//        Map<Integer, String> map = persons
//                .stream()
//                .collect(Collectors.toMap(
//                        p -> p.age,
//                        p -> p.name,
//                        (name1, name2) -> name1  + ";" + name2
//                ));
//        System.out.println(map);


//        IntSummaryStatistics ageSummary = persons
//                        .stream()
//                        .collect(Collectors.summarizingInt(p -> p.age));
//        System.out.println(ageSummary);

//        Double averageAge = persons
//                .stream()
//                .collect(Collectors.averagingDouble(p -> p.age));
//
//        System.out.println(averageAge);

//        List<ukr.Person> filtered = persons
//                .stream()
//                .filter(p -> p.name.startsWith("I"))
//                .collect(Collectors.toList());
//        System.out.println(filtered);
//
//        Map<Integer, List<ukr.Person>> personsByAge = persons
//                .stream()
//                .collect(Collectors.groupingBy(p -> p.age));
//
//        personsByAge
//                .forEach((age, p) -> System.out.format("age %s: %s\n", age, p));
//
//        System.out.println(personsByAge);

//        ------------------------------------------------------------------------------------------

//        Stream<String> stream = Stream.of("dd2", "aa2", "bb1", "bb3", "cc")
//                .filter(s -> s.startsWith("b"));
//        stream.forEach(System.out::println);
//        stream.forEach(System.out::println);


//        Supplier<Stream<String>> streamSupplier =
//                () -> Stream.of("dd2", "aa2", "bb1", "bb3", "cc")
//                .filter(s -> s.startsWith("b"));
//
//        streamSupplier.get().forEach(System.out::println);
//        streamSupplier.get().forEach(System.out::println);

//        Stream.of("dd2", "aa2", "bb4", "bb3", "cc4")
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("b");
//                })
//                .sorted((s1, s2) -> {
//                    System.out.printf("sort: %s; %s\n", s1, s2);
//                    return s1.compareTo(s2);
//                })
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("forEach: " + s));
//
//        Stream.of("dd2", "aa2", "bb1", "bb3", "cc4")
//                .filter(s -> {
//                    System.out.println("Filtr: " + s);
//                    return true;
//                })
////                ;
//                .forEach(s -> System.out.println("forEach: " + s));

//        Stream.of(1.0, 2.0, 3.0)
//                .mapToInt(Double::intValue)
//                .mapToObj(i -> "с" + i)
//                .forEach(System.out::println);

//        IntStream.range(1, 4)
//                .mapToObj(i -> i + 1 + "ccc" + i)
//                .forEach(System.out::println);

//        Stream.of("cc1", "cc2", "cc3")
//                .map(s -> s.substring(2))
//                .mapToInt(Integer::parseInt)
//                .max()
//                .ifPresent(System.out::println);

//        Arrays.stream(new int[]{1, 2, 3})
//                .map(n -> 2 * n + 1)
//                .average()
//                .ifPresent(System.out::println);

//        IntStream.range(8,12).forEach(System.out::println);

//        Stream.of("сс1", "сс2", "сс3")
//                .findFirst()
//                .ifPresent(System.out::println);

//        Arrays.asList("сс1", "сс2", "сс3")
//                .stream()
//                .findAny()
////                .findFirst()
//                .ifPresent(System.out::println);

//        List<String> mList = Arrays.asList("aa1", "cc2", "cc1", "aa2", "bb1");
//
//        mList
//                .stream()
////                .filter(s -> s.startsWith("c"))
////                .map(String::toUpperCase)
//                .map(s -> s.charAt(2))
////                .sorted()
//                .forEach(System.out::println);
    }
}
