package tuturialspoint;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by a on 12.05.16.
 */
public class Tp {
    public static void main(String[] args) {
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
        List<String> filtered = strings.stream().filter(s -> s.isEmpty()).collect(Collectors.toList());
        for (String s : strings) System.out.println(s);

        System.out.println("".isEmpty());
    }
}
